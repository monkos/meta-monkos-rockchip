SRCBRANCH ?= "rockchip-2.4.8"
SRC_URI = "git://github.com/FireflyTeam/libdrm-rockchip.git;branch=${SRCBRANCH}"
SRCREV = "d2f498d648bf6e36c3db45d18f6d49044ad2ff9e"
